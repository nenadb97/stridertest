const assert = require('assert');
const chai = require("chai");
const chaiHttp = require("chai-http");

const app = require("../app");

chai.use(chaiHttp);

describe('Testing', function() {

    it('should return -1 when the value is not present', function() {
        assert.equal(-1, [1,2,3].indexOf(4));
    });

    it('should return something on /users', function(done) {
        chai.request(app)
            .get("/users")
            .end(function (err, res) {
                chai.expect(res.text).to.be.equal("respond with a resource");
                chai.expect(res.status).to.be.equal(200);
                done();
            });
    });

    it('should return something on /', function(done) {
        chai.request(app)
            .get("/")
            .end(function (err, res) {
                chai.expect(res.status).to.be.equal(200);
                done();
            });
    });

    it('should return 404 on nonexitent', function(done) {
        chai.request(app)
            .get("/test")
            .end(function (err, res) {
                chai.expect(res.status).to.be.equal(404);
                done();
            });
    });
});